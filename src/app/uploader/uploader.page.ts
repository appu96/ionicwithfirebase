import { OnInit, Component } from '@angular/core';
import { Http } from '@angular/http';
import { UserService } from '../user.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { firestore } from 'firebase/app';

@Component({
  selector: 'app-uploader',
  templateUrl: './uploader.page.html',
  styleUrls: ['./uploader.page.scss']
})

export class UploaderPage implements OnInit {
  imageURL: string;
  desc: string;

  constructor(public http: Http,
    public user: UserService,
    public afstore: AngularFirestore) { }

  ngOnInit() {
  }
   
  createPoster() {
    const image = this.imageURL
    const desc = this.desc
    
    this.afstore.doc('user/${this.user.getUser()}').set({
      posts: firestore.FieldValue.arrayUnion({
        image,
        desc
      })
    });
  }



  fileChanged(event) {
    const files = event.target.files;

    const data = new FormData();
    data.append('file', files[0]);
    data.append('UPLOADCARE_STORE', '1');
    data.append('UPLOADCARE_PUB_KEY', '77bee180d30a9e0c5026');


    this.http.post('https://upload.uploadcare.com/base/', data)
    .subscribe(event => {
        console.log(event);
        this.imageURL = event.json().file;
    });
  }

}
